package com.sshakya.xhift10;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.github.angads25.filepicker.controller.DialogSelectionListener;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.github.se_bastiaan.torrentstream.StreamStatus;
import com.github.se_bastiaan.torrentstream.Torrent;
import com.github.se_bastiaan.torrentstream.TorrentOptions;
import com.github.se_bastiaan.torrentstream.TorrentStream;
import com.github.se_bastiaan.torrentstream.listeners.TorrentListener;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.sshakya.xhift10.bencoder.BencodeDictionary;
import com.sshakya.xhift10.torrent.TorrentFile;
import com.sshakya.xhift10.tracker.XhiftHTTPTrackerServer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

@SuppressLint("SetTextI18n")
public class MainActivity extends AppCompatActivity implements TorrentListener {


    /* Static Variable */
    static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
    private static final String TORRENT = "Torrent";

    /* Android widgets */
    /*toolbar*/
    private Toolbar toolbar;
    /* Upload widgets */
    private ImageView ivUploadQr;
    private TextView tvUploadParams;

    /*Download widgets*/
    private ImageView ivDownloadQr;
    private TextView tvDownloadParams;
    private ProgressBar progressBar;

    /*Floating action button*/
    private FloatingActionButton fabDownloadFile;
    private FloatingActionButton fabUploadFile;

    private Button button;

    /*Dialog properties*/
    private DialogProperties properties;

    /* Torrent */
    private TorrentStream torrentStream;
    private TorrentFile torrentFile;
    private String streamUrl;
    private XhiftHTTPTrackerServer server;
    private Boolean uploadFlag=false;
    private String torrentLocation;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        /* Initialize widgets */
        /**
         *Setup the Toolbar
         */
        toolbar =(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        /*Upload widgets */
        ivUploadQr =(ImageView)findViewById(R.id.ivUploadQr);
        tvUploadParams =(TextView)findViewById(R.id.tvUploadParams);

        /*Download widgets*/
        ivDownloadQr=(ImageView)findViewById(R.id.ivDownloadQr);
        tvDownloadParams=(TextView)findViewById(R.id.tvDownloadParams);
        progressBar =(ProgressBar)findViewById(R.id.progress);

        /*floating action buttons */
        fabDownloadFile = (FloatingActionButton)findViewById(R.id.fabDownloadFile);
        fabUploadFile=(FloatingActionButton)findViewById(R.id.fabUploadFile);
        button = (Button)findViewById(R.id.button);

        /* Set dialog properties for file selector */
        properties=new DialogProperties();
        properties.selection_mode= DialogConfigs.SINGLE_MODE;
        properties.selection_type=DialogConfigs.FILE_SELECT;
        properties.root=new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions=null;

        progressBar.setMax(100);

        /* urldecode for magnetlink*/
        String action = getIntent().getAction();
        Uri data = getIntent().getData();
        if (action != null && action.equals(Intent.ACTION_VIEW) && data != null) {
            try {
                streamUrl = URLDecoder.decode(data.toString(), "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        /*fab upload button on click list+ener */

        fabUploadFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"Select File", Toast.LENGTH_SHORT).show();
                FilePickerDialog dialog = new FilePickerDialog(context,properties);
                dialog.setDialogSelectionListener(new DialogSelectionListener() {
                    @Override
                    public void onSelectedFilePaths(String[] files) {
                        StringBuilder builder = new StringBuilder();
                        for(String s:files){
                            builder.append(s);
                        }

                        String fileNameString=builder.toString();
                        Toast.makeText(context,fileNameString,Toast.LENGTH_SHORT).show();
                        try {
                            WifiManager wifiMan = (WifiManager)getSystemService(Context.WIFI_SERVICE);
                            WifiInfo wifiInf = wifiMan.getConnectionInfo();
                            int ipAddress = wifiInf.getIpAddress();
                            String ip = String.format("%d.%d.%d.%d", (ipAddress & 0xff),(ipAddress >> 8 & 0xff),(ipAddress >> 16 & 0xff),(ipAddress >> 24 & 0xff));

                            torrentFile = new TorrentFile(fileNameString, "http://"+ip+":6969/announce");
                            String magnetLink = torrentFile.getMagnetLink();
                            Log.d("magnet",magnetLink);
                            BencodeDictionary torrent_dict = torrentFile.getTorrentDict();

                            String filename = "temp.torrent";
                            FileOutputStream outputStream;

                            try {
                                outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
                                outputStream.write(torrent_dict.getBencodeBytes());
                                outputStream.close();

                                torrentLocation = "file://" + getFileStreamPath(filename).getAbsolutePath();

                                Log.d("Torrent creation", torrentLocation);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            final TorrentOptions torrentOptions = new TorrentOptions.Builder()
                                    .saveLocation(fileNameString.substring(0,fileNameString.lastIndexOf('/')))
                                    .removeFilesAfterStop(true)
                                    .build();

                            torrentStream = TorrentStream.init(torrentOptions);
                            torrentStream.addListener((TorrentListener) context);

                            Toast.makeText(context,magnetLink,Toast.LENGTH_SHORT).show();
                            ivUploadQr.setImageBitmap(getQrCode(magnetLink,500,500));
                            tvUploadParams.setText("Seeding...");
                            uploadFlag=true;
                            Toast.makeText(context,"Select Stream Now",Toast.LENGTH_SHORT).show();


                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
                dialog.show();
            }
        });

        /* fab Download Button */
        fabDownloadFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(ACTION_SCAN);
                    intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
                    startActivityForResult(intent, 0);
                    Toast.makeText(context,getStreamUrl(),Toast.LENGTH_SHORT).show();
                } catch (ActivityNotFoundException anfe){
                    showDialog(MainActivity.this,"No Scanner Found","Download a scanner code activity?","Yes","No").show();

                }

                final TorrentOptions torrentOptions = new TorrentOptions.Builder()
                        .saveLocation(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS))
                        .removeFilesAfterStop(true)
                        .build();

                torrentStream = TorrentStream.init(torrentOptions);
                torrentStream.addListener((TorrentListener) context);

            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setProgress(0);
                if(torrentStream.isStreaming()) {
                    torrentStream.stopStream();
                    button.setText("Start stream");
                    return;
                }
                if(uploadFlag) {
                    torrentStream.startStream(torrentLocation);
                } else {
                    torrentStream.startStream(streamUrl);
                }
                button.setText("Stop stream");
            }
        });



    }
    @Override
    protected void onResume() {
        super.onResume();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }
        try {
            if(server ==null) {
                server = new XhiftHTTPTrackerServer();
            }
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
            server = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        server.quit();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(server != null) {
            server.stop();
            server = null;
        }
    }


    @Override
    public void onStreamPrepared(Torrent torrent) {
        Log.d(TORRENT, "OnStreamPrepared");

        torrent.startDownload();

    }

    @Override
    public void onStreamStarted(Torrent torrent) {
        torrent.getTorrentHandle().resume();
        torrent.resume();
        Log.d(TORRENT, "onStreamStarted");
    }

    @Override
    public void onStreamError(Torrent torrent, Exception e) {
        Log.e(TORRENT, "onStreamError", e);
        button.setText("Start stream");
    }

    @Override
    public void onStreamReady(Torrent torrent) {
        torrent.getTorrentHandle().resume();
        torrent.resume();


    }

    @Override
    public void onStreamProgress(Torrent torrent, StreamStatus status) {
        torrent.getTorrentHandle().resume();
        torrent.resume();
        if( progressBar.getProgress() < 100 ) {
            Log.d(TORRENT, "Progress: " + status.bufferProgress);

            progressBar.setProgress((int)status.progress);
            tvDownloadParams.setText("Download Speed " + status.downloadSpeed /1024+"\n Seeds: "+status.seeds);

        }

    }

    @Override
    public void onStreamStopped() {
        Log.d(TORRENT, "onStreamStopped");
    }
     /* End torrent seed and downloading */

    /* on ActivityResult method */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(requestCode==0) {
            if (resultCode == RESULT_OK) {
                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                Toast.makeText(context, "Content: " + contents + " Format: " + format, Toast.LENGTH_SHORT).show();
                Log.d("magnetLink", contents);
                setStreamUrl(contents);
                ivDownloadQr.setImageBitmap(getQrCode(contents,500,500));


            }
        }
    }

    private static AlertDialog showDialog(final Activity act, CharSequence title, CharSequence message, CharSequence buttonYes, CharSequence buttonNo) {
        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act.getApplicationContext());
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Uri uri = Uri.parse("market://search?q=pname:" + "com.google.zxing.client.android");
                Intent intent = new Intent(Intent.ACTION_VIEW,uri);
                try{
                    act.startActivity(intent);
                } catch (ActivityNotFoundException anfe){

                }

            }
        });
        downloadDialog.setNegativeButton(buttonNo, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        return downloadDialog.show();
    }

    //Barcode static method
    public  static Bitmap getQrCode(String data, int width, int height){
        com.google.zxing.Writer writer = new QRCodeWriter();
        // String finaldata = Uri.encode(data, "utf-8");
        try {
            BitMatrix bm = writer.encode(data, BarcodeFormat.QR_CODE, width, height);

            Bitmap bitmap = Bitmap.createBitmap(width, height,
                    Bitmap.Config.ARGB_8888);

            for (int i = 0; i < width; i++) {// width
                for (int j = 0; j < height; j++) {// height
                    bitmap.setPixel(i, j, bm.get(i, j) ? Color.BLACK
                            : Color.WHITE);
                }
            }
            return bitmap;
        }catch (WriterException e){
            e.printStackTrace();
        }
        return null;
    }

    //StreamUrl pojo
    public void setStreamUrl(String streamUrl){
        this.streamUrl=streamUrl;
    }
    public String getStreamUrl(){
        return this.streamUrl;
    }
}
