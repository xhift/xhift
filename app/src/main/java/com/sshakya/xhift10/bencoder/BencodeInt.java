package com.sshakya.xhift10.bencoder;

import android.annotation.TargetApi;
import android.os.Build;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by sshakya on 8/12/16.
 */

public class BencodeInt implements BencodeDataType {

    private final long value;

    final static String UTF_8 = "UTF-8";

    public BencodeInt(int x) {
        this.value = x;
    }

    public BencodeInt(long x) {
        this.value = x;
    }

    @Override
    public String getBencode() {
        return "i" + value + "e";
    }

    @Override
    public byte[] getBencodeBytes() {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            out.write("i".getBytes(Charset.forName(UTF_8)));
            out.write((value+"").getBytes( Charset.forName(UTF_8)));
            out.write("e".getBytes(Charset.forName(UTF_8)));
        } catch (IOException ex) {
            Logger.getLogger(BencodeInt.class.getName()).log(Level.SEVERE, null, ex);
        }
        return out.toByteArray();
    }

    public long getValue() {
        return value;
    }

    @Override
    public String getJSON() {
        return Long.toString(value);
    }

}