package com.sshakya.xhift10.bencoder;

import android.annotation.TargetApi;
import android.os.Build;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by sshakya on 8/12/16.
 */

public class BencodeString implements BencodeDataType {

    private final byte[] value;

    final static String UTF_8 = "UTF-8";

    public BencodeString(String s) {
        this.value = s.getBytes( Charset.forName(UTF_8));
    }

    public BencodeString(byte[] s) {
        this.value = s;
    }

    @Override
    public String getBencode() {
        return this.value.length + new String(value,  Charset.forName(UTF_8));
    }

    @Override
    public byte[] getBencodeBytes() {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            out.write((this.value.length + ":").getBytes( Charset.forName(UTF_8)));
            out.write(this.value);
        } catch (IOException ex) {
            Logger.getLogger(BencodeString.class.getName()).log(Level.SEVERE, null, ex);
        }
        return out.toByteArray();
    }

    public String getValue() {
        return new String(value,  Charset.forName(UTF_8));
    }

    public byte[] getValueBytes() {
        return value;
    }

    @Override
    public String getJSON() {
        return "\"" + new String(value,  Charset.forName(UTF_8)) + "\"";
    }

}

