package com.sshakya.xhift10.bencoder;

import android.annotation.TargetApi;
import android.os.Build;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by sshakya on 8/12/16.
 */

public class BencodeDictionary extends HashMap<String, BencodeDataType> implements BencodeDataType {

    public void put(String key, int x) {
        this.put(key, new BencodeInt(x));
    }

    public void put(String key, long x) {
        this.put(key, new BencodeInt(x));
    }

    public void put(String key, String s) {
        this.put(key, new BencodeString(s));
    }

    public void put(String key, byte[] s) {
        this.put(key, new BencodeString(s));
    }

    final static String UTF_8 = "UTF-8";
    @Override
    public String getBencode() {
        String temp = "d";

        Object[] keys = this.keySet().toArray();
        Arrays.sort(keys);

        for(Object key : keys) {
            temp += new BencodeString((String) key).getBencode();
            temp += this.get((String) key).getBencode();
        }

        temp += "e";

        return temp;
    }

    @Override
    public byte[] getBencodeBytes() {
        ByteArrayOutputStream out = new ByteArrayOutputStream( );
        try {
            out.write("d".getBytes( Charset.forName(UTF_8)));

            Object[] keys = this.keySet().toArray();
            Arrays.sort(keys);

            for(Object key: keys) {
                out.write(new BencodeString((String) key).getBencodeBytes());
                out.write(this.get((String) key).getBencodeBytes());
            }
            out.write("e".getBytes( Charset.forName(UTF_8)));
        } catch (IOException ex) {

            Logger.getLogger(BencodeDictionary.class.getName()).log(Level.SEVERE, null, ex);
        }
        return out.toByteArray();
    }

    @Override
    public String getJSON() {
        String temp = "{";

        Object[] keys = this.keySet().toArray();
        Arrays.sort(keys);

        for(Object key : keys) {
            temp += new BencodeString((String) key).getJSON();
            temp += ":";
            temp += this.get((String) key).getJSON();
            temp += ",";
        }

        temp = temp.substring(0, temp.length() - 1 );

        temp += "}";

        return temp;
    }

}
