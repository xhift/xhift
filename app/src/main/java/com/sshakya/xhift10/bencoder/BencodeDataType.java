package com.sshakya.xhift10.bencoder;

/**
 *
 * @author squgeim
 */
public interface BencodeDataType {
    
    public String getBencode();
    public String getJSON();
    public byte[] getBencodeBytes();

}
