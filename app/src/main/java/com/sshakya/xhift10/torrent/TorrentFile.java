package com.sshakya.xhift10.torrent;



import android.annotation.TargetApi;
import android.os.Build;

import com.sshakya.xhift10.bencoder.BencodeDictionary;
import com.sshakya.xhift10.bencoder.BencodeList;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by sshakya on 7/19/16.
 */

public class TorrentFile {

    private FileInputStream fs = null;
    private File file = null;

    private String name = null;
    private String announce = null;
    private BencodeList announce_list = null;
    private BencodeDictionary info = null;

    private int piece_length;

    public TorrentFile(String filepath, String announce) throws FileNotFoundException {

        file = new File(filepath);

        if (!file.exists()) {
            throw new FileNotFoundException();
        }

        fs = new FileInputStream(file);

        this.name = file.getName();
        this.announce = announce;

        this.piece_length = getPieceLength();

        if (file.isDirectory()) {
            getInfoForDir();
        } else if (file.isFile()) {
            getInfoForFile();
        }
    }

    private int getPieceLength() {
        /*
        If the file size is less than 1 MB, the pieces will be 32KB each,
        if the file size is between 1 MB and 5 MB, the pieces will be 512KB each,
        else the piece length will be 1 MB each.
         */

        long file_length = file.length() / 1024;

        if (file_length < 1024) {
            return 32 * 1024;
        } else if (file_length < 5 * 1024) {
            return 512 * 1024;
        } else {
            return 1024 * 1024;
        }
    }

    private void getInfoForFile() {
        long length;
        String md5sum = null;
        //int piece_length = 1048576; // 1024 * 1024
        ByteArrayOutputStream pieces_stream = null;

        length = file.length();

        try {
            pieces_stream = new ByteArrayOutputStream();
            md5sum = getPiecesHashes(pieces_stream);
        } catch (NoSuchAlgorithmException | IOException ex) {
            Logger.getLogger(TorrentFile.class.getName()).log(Level.SEVERE, null, ex);
        }
//
//        System.out.println("name: " + name);
//        System.out.println("length: " + length);
//        System.out.println("md5sum: " + md5sum);
//        System.out.println("piece length: " + piece_length);
//        System.out.println("pieces: " + pieces);

        this.info = new BencodeDictionary();
        info.put("name", this.name);
        info.put("length", length);
        info.put("md5sum", md5sum);
        info.put("piece length", piece_length);
        info.put("pieces", pieces_stream.toByteArray());
    }

    private void getInfoForDir() {
    }

    private String getPiecesHashes(ByteArrayOutputStream pieces) throws NoSuchAlgorithmException, IOException {

        //ArrayList<byte[]> pieces = new ArrayList<>();
        byte[] buffer = new byte[1024];

        MessageDigest md5digest = MessageDigest.getInstance("MD5");
        MessageDigest sha1digest = MessageDigest.getInstance("SHA1");
        int numRead;
        long piece_byte_count = 0;
        do {
            numRead = fs.read(buffer);
            if (numRead > 0) {
                md5digest.update(buffer, 0, numRead);
                sha1digest.update(buffer, 0, numRead);
                piece_byte_count += 1;
            }
            if (piece_byte_count == (piece_length / 1024)) {
                byte[] pb = sha1digest.digest();

                //pieces.add(pb);
                piece_byte_count = 0;
//
//                String piece_sha1 = "";
//                for (int i=0; i < pb.length; i++) {
//                    piece_sha1 += Integer.toString( ( pb[i] & 0xff ) + 0x100, 16).substring( 1 );
//                }
                pieces.write(pb);
                //pieces_list.add(new String(pb));
            }
        } while (numRead != -1);
        byte[] b = md5digest.digest();
        byte[] pb = sha1digest.digest();
        //pieces.add(sha1digest.digest());
        String file_md5 = "";
        for (int i = 0; i < b.length; i++) {
            file_md5 += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
        }
        pieces.write(pb);

        return file_md5;
    }

    public BencodeDictionary getTorrentDict() {
        BencodeDictionary torrent = new BencodeDictionary();
        torrent.put("announce", announce);
        if (announce_list != null) {
            torrent.put("announce-list", announce_list);
        }
        torrent.put("created by", "Xhift Torrent Creator");
        torrent.put("creation date", (long) System.currentTimeMillis() / 1000L);
        torrent.put("info", info);

        return torrent;
    }

    private String getInfoHash() {
        try {
            MessageDigest sha1digest = MessageDigest.getInstance("SHA1");
            sha1digest.update(info.getBencodeBytes());
            byte[] sha1_bytes = sha1digest.digest();
            String sha1 = "";
            for (int i = 0; i < sha1_bytes.length; i++) {
                sha1 += Integer.toString((sha1_bytes[i] & 0xff) + 0x100, 16).substring(1);
            }
            return sha1;
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(TorrentFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    public String getMagnetLink() {
        String link = "magnet:?xt=urn:btih:";
        link += getInfoHash();
        try {
            link += "&dn=" + URLEncoder.encode(name, "UTF-8");
            link += "&tr=" + URLEncoder.encode(announce, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(TorrentFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        return link;
    }
}
