package com.sshakya.xhift10.tracker;

import com.sshakya.xhift10.bencoder.BencodeDataType;
import com.sshakya.xhift10.bencoder.BencodeString;

import java.io.IOException;
import java.util.HashMap;

import fi.iki.elonen.NanoHTTPD;

/**
 * Created by sshakya on 8/21/16.
 */
public class XhiftHTTPTrackerServer extends NanoHTTPD {

    XhiftTracker tr = XhiftTracker.getInstance();

    public XhiftHTTPTrackerServer() throws IOException {
        super(6969); // Start http server at port 6969

    }



    @Override
    public Response serve(IHTTPSession session) {
        String url = session.getUri();
        BencodeString remote_ip = new BencodeString(session.getRemoteIpAddress());

        HashMap<String, String> parameters = (HashMap<String, String>) session.getParms();

        System.out.println(remote_ip + ": " + url);
        System.out.println(parameters);

        if (!"/announce".equals(url)) {
            return newFixedLengthResponse(Response.Status.BAD_REQUEST, MIME_HTML, "<html><head><title>Invalid Request</title></head></html>");
        }

        BencodeString info_hash = new BencodeString(parameters.get("info_hash"));
        BencodeString peer_id = new BencodeString(parameters.get("peer_id"));

        if ("/announce".equals(url)) {
            int port = new Integer(parameters.get("port"));
            int uploaded = new Integer(parameters.get("uploaded"));
            int left = new Integer(parameters.get("downloaded"));
            String ip = parameters.get("ip");
            if (ip != null) {
                remote_ip = new BencodeString(ip);
            }
            tr.insertPeer(info_hash, peer_id, remote_ip, port);
        }

        BencodeDataType response = tr.getResponse(info_hash, peer_id);

        return newFixedLengthResponse(Response.Status.OK, MIME_PLAINTEXT, response.getBencode());
    }

    public void quit() {
        tr.kill();
        this.stop();
    }

}
